package com.zuitt;

public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact();
        contact1.setName("Miko");
        contact1.setContactNumber("123456789");
        contact1.setAddress("Philippines");

        phonebook.setContacts(contact1);

        if (phonebook.getContacts().isEmpty()) {
            System.out.println("The phonebook is currently empty.");
        } else {
            phonebook.getContacts().forEach(contact -> {
                System.out.println(contact.getName());
                System.out.println(contact.getContactNumber());
                System.out.println(contact.getAddress());
            });
        }
    }
}
