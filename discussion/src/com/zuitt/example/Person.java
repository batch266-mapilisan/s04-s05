package com.zuitt.example;

public class Person implements Actions {
    public void sleep() {
        System.out.println("Sleeping......");
    }

    public void run() {
        System.out.println("Running.....");
    }
}
